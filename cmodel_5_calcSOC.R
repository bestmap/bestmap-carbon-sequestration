################################################################################
# order of scripts                                                             #
# 1. cmodel_1_prepSOCbase.R                                                    #
# 2. cmodel_2_calcSOCbase.R                                                    #
# 3. cmodel_3_getcropgroups.R                                                  #
# 4. cmodel_4_calcSOClu.R                                                      #
# 5. cmodel_5_calcSOC.R                                                        #
#                                                                              #
################################################################################


################################################################################
#                                                                              #
#     5     cmodel_5_calcSOC.R                                                 #
#                                                                              #
################################################################################
#                                                                              #
# note:    - calculates the SOC map under AEM and alternative nows             #
#                                                                              #
# reads:   - parcelSOClu.dbf (produced with cmodel_4_calcSOClu.R)              #
#                                                                              #
# writes:  - parcelSOCcurr.dbf (under current AEM adoption)                    #
#          - parcelSOCalt.dbf (under alternative-now scenarios)                #
#                                                                              #
# written by F. Langerwisch                                                    #
# 20220511                                                                     #
#                                                                              #
################################################################################

rm(list=ls(all=TRUE)); gc()

################################################################################
#                load additional Packages                                      #
################################################################################
library(foreign)     # for reading dbf

################################################################################
#                set paths                                                     #
################################################################################
path.lpis <- 'C:/Users/schneika/Documents/Bestmap/CS_data/CZ_LPIS_2015-19/'
path.analysis <- 'C:/Users/schneika/Documents/Bestmap/CarbonModelCZ/Invest/'
path.invest <- 'C:/Users/schneika/Documents/Bestmap/CarbonModelCZ/Invest/'

################################################################################
#                read in parameters                                            #
################################################################################
parameter <- read.table(paste(path.invest,'parameter.txt', sep=''), header=T)

p.aem.covercrops <- parameter[which(parameter[,'name']=='p.aem.covercrops'),'value']
p.aem.organic <- parameter[which(parameter[,'name']=='p.aem.organic'),'value']
p.aem.maintgrass <- parameter[which(parameter[,'name']=='p.aem.maintgrass'),'value']
p.aem.buffer <- parameter[which(parameter[,'name']=='p.aem.buffer'),'value']
p.aem.luconvgrass <- parameter[which(parameter[,'name']=='p.aem.luconvgrass'),'value']
p.aem.luconvforest <- parameter[which(parameter[,'name']=='p.aem.luconvforest'),'value']

#name                 value
p.aem.covercrops    # 0.10
p.aem.organic       # 0.00
p.aem.maintgrass    # 0.00
p.aem.buffer        # 0.40
p.aem.luconvgrass   # 0.40
p.aem.luconvforest  # 0.50

################################################################################
#                read in files                                                 #  
################################################################################
#----- SOC
parcel.SOCmap.df <- read.dbf(paste(path.invest, 'parcelSOClu.dbf', sep=''))
parcel.SOCmap <- as.matrix(parcel.SOCmap.df)

#----- LPIS
lpis_2019 <- get(load(file=paste(path.lpis, 'lpis_2019.Rdata', sep='')))

#---- AEM
parceldata.AEM.frac.df <- read.dbf(paste(path.invest, 'parcelAEMfrac.dbf', sep=''))
parceldata.AEM.frac <- as.matrix(parceldata.AEM.frac.df)

################################################################################
#                extract information about land use type                 #  
################################################################################
#for CZ KULTURAKOD KULTURA   name.en
#       "R"        "2"       "standard arable land"
#       "T"        "7"       "permanent grassland"
whichparcels.arable <- which(lpis_2019[,'KULTURA']==2)
whichparcels.permgrass <- which(lpis_2019[,'KULTURA']==7)
rm(lpis_2019); gc()

################################################################################
#                calculate SOC depending on current AEM                        #  
################################################################################
parcel.SOCcurr.df <- cbind(parcel.SOCmap[,'NKOD_DPB'], parcel.SOCmap[,'ID_UZ'], rep(NA, dim(parcel.SOCmap)[1]), rep(NA, dim(parcel.SOCmap)[1]), rep(NA, dim(parcel.SOCmap)[1]))
colnames(parcel.SOCcurr.df) <- c('NKOD_DPB','ID_UZ', 'SOCcurr', 'lumincur','fraccur')
parcel.SOCcurr <- as.matrix(parcel.SOCcurr.df)

#---- calculate factor to change depending on AEM
#some AEM are on whole farm, so sum frac can be larger 1, 
adaptionfactor.curr <- 1+
                  ((p.aem.covercrops  * as.double(parceldata.AEM.frac[,'AEM_cc']))    +
                  (p.aem.organic      * as.double(parceldata.AEM.frac[,'AEM_org']))   +
                  (p.aem.maintgrass   * as.double(parceldata.AEM.frac[,'AEM_grm']))   +
                  (p.aem.buffer       * as.double(parceldata.AEM.frac[,'AEM_buf']))   +
                  (p.aem.luconvgrass  * as.double(parceldata.AEM.frac[,'AEM_convgr']))+
                  (p.aem.luconvforest * as.double(parceldata.AEM.frac[,'AEM_convfo'])))
parcel.SOCcurr[,'SOCcurr'] <- as.double(parcel.SOCmap[,'SOC']) * adaptionfactor.curr

#---- calculate factor to change depending on AEM for permanent grassland and correct for perm grassland
#- leave out AEM_grm, otherwise the effect of permanent grassland it added twice
adaptionfactor.curr.pg <- 1+
                  ((p.aem.covercrops  * as.double(parceldata.AEM.frac[,'AEM_cc']))    +
                  (p.aem.organic      * as.double(parceldata.AEM.frac[,'AEM_org']))   +
                  (p.aem.buffer       * as.double(parceldata.AEM.frac[,'AEM_buf']))   +
                  (p.aem.luconvgrass  * as.double(parceldata.AEM.frac[,'AEM_convgr']))+
                  (p.aem.luconvforest * as.double(parceldata.AEM.frac[,'AEM_convfo'])))
parcel.SOCcurr[whichparcels.permgrass,'SOCcurr'] <- as.double(parcel.SOCmap[whichparcels.permgrass,'SOC']) * adaptionfactor.curr.pg[whichparcels.permgrass]

#----- calculuate the difference between base and current SOC
parcel.SOCcurr[,'lumincur'] <- as.double(parcel.SOCmap[,'SOC']) - as.double(parcel.SOCcurr[,'SOCcurr'])
parcel.SOCcurr[,'fraccur'] <- (as.double(parcel.SOCmap[,'SOC'])*100/as.double(parcel.SOCcurr[,'SOCcurr']))-100

#----- round values
parcel.SOCcurr[,'SOCcurr'] <- round(as.double(parcel.SOCcurr[,'SOCcurr']), 1)
parcel.SOCcurr[,'lumincur'] <- round(as.double(parcel.SOCcurr[,'lumincur']), 2)
parcel.SOCcurr[,'fraccur'] <- round(as.double(parcel.SOCcurr[,'fraccur']), 1)

################################################################################
#                calculate SOC depending on alternative AEM                    #  
################################################################################
parcel.SOCalt.cc.df <- cbind(parcel.SOCmap[,'NKOD_DPB'], parcel.SOCmap[,'ID_UZ'], rep(NA, dim(parcel.SOCmap)[1]), rep(NA, dim(parcel.SOCmap)[1]), rep(NA, dim(parcel.SOCmap)[1]))
colnames(parcel.SOCalt.cc.df) <- c('NKOD_DPB','ID_UZ', 'SOCalt', 'altmincur', 'fraccur')
parcel.SOCalt.cc <- as.matrix(parcel.SOCalt.cc.df)

#cover crops every where (on arable farms, where there is not cc yet)
#parceldata.AEM.ccfull <- rep(0, dim(parcel.SOCmap)[1])
#parceldata.AEM.ccfull[whichparcels.arable] <- 1

adaptionfactor.alt <- 1 #+
    # ((p.aem.covercrops   * parceldata.AEM.ccfull)    +
    #  (p.aem.organic      * as.double(parceldata.AEM.frac[,'AEM_org']))   +
    #  (p.aem.maintgrass   * as.double(parceldata.AEM.frac[,'AEM_grm']))   +
    #  (p.aem.buffer       * as.double(parceldata.AEM.frac[,'AEM_buf']))   +
    #  (p.aem.luconvgrass  * as.double(parceldata.AEM.frac[,'AEM_convgr']))+
    #  (p.aem.luconvforest * as.double(parceldata.AEM.frac[,'AEM_convfo'])))

parcel.SOCalt.cc[,'SOCalt'] <- as.double(parcel.SOCmap[,'SOC']) * adaptionfactor.alt

#---- calculate factor to change depending on AEM for permanent grassland and correct for perm grassland
#- leave out AEM_grm, otherwise the effect of permanent grassland it added twice
# adaptionfactor.alt.pg <- 1+
#     ((p.aem.covercrops   * parceldata.AEM.ccfull)    +
#      (p.aem.organic      * as.double(parceldata.AEM.frac[,'AEM_org']))   +
#      (p.aem.buffer       * as.double(parceldata.AEM.frac[,'AEM_buf']))   +
#      (p.aem.luconvgrass  * as.double(parceldata.AEM.frac[,'AEM_convgr']))+
#      (p.aem.luconvforest * as.double(parceldata.AEM.frac[,'AEM_convfo'])))
# parcel.SOCalt.cc[whichparcels.permgrass,'SOCalt'] <- as.double(parcel.SOCmap[whichparcels.permgrass,'SOC']) * adaptionfactor.alt.pg[whichparcels.permgrass]

#----- calculuate the difference between base and current SOC
parcel.SOCalt.cc[,'altmincur'] <- as.double(parcel.SOCalt.cc[,'SOCalt']) - as.double(parcel.SOCcurr[,'SOCcurr'])
parcel.SOCalt.cc[,'fraccur'] <- (as.double(parcel.SOCalt.cc[,'SOCalt'])*100/as.double(parcel.SOCcurr[,'SOCcurr']))-100

#----- round values
parcel.SOCalt.cc[,'SOCalt'] <- round(as.double(parcel.SOCalt.cc[,'SOCalt']), 1)
parcel.SOCalt.cc[,'altmincur'] <- round(as.double(parcel.SOCalt.cc[,'altmincur']), 2)
parcel.SOCalt.cc[,'fraccur'] <- round(as.double(parcel.SOCalt.cc[,'fraccur']), 1)

################################################################################
#                sanity check                                                  #
################################################################################
if(min(as.double(parcel.SOCcurr[,'SOCcurr']), na.rm=T)<0) print('SOCcurr below 0')
if(min(as.double(parcel.SOCalt.cc[,'SOCalt']), na.rm=T)<0) print('SOCalt below 0')
if(max(as.double(parcel.SOCcurr[,'SOCcurr'])/as.double(parcel.SOCmap[,'SOC']), na.rm=T)>2) print('SOCcurr far too high')
if(max(as.double(parcel.SOCalt.cc[,'SOCalt'])/as.double(parcel.SOCcurr[,'SOCcurr']), na.rm=T)>2) print('SOCcurr far too high')

################################################################################
#                safe tables                                                   #
################################################################################
parcel.SOCcurr.df <- as.data.frame(parcel.SOCcurr)
write.dbf(parcel.SOCcurr.df, paste(path.invest, 'parcel.SOCcurr.dbf', sep='') )
#parcel.SOCcurr.new <- read.dbf(paste(path.invest, 'parcel.SOCcurr.dbf', sep=''))

parcel.SOCalt.cc.df <- as.data.frame(parcel.SOCalt.cc)
write.dbf(parcel.SOCalt.cc.df, paste(path.invest, 'parcel.SOCalt.noAEM.dbf', sep='') )
#parcel.SOCalt.new <- read.dbf(paste(path.invest, 'parcel.SOCalt.cc.dbf', sep=''))

################ EOF ###########################################################
